import 'package:movieapp/model/genre.dart';
import 'package:movieapp/model/tvshow/genre_tvshow.dart';

class GenreTvshowResponse {
  final List<GenreTvShow> genres;
  final String error;

  GenreTvshowResponse(this.genres, this.error);

  GenreTvshowResponse.fromJson(Map<String, dynamic> json)
      : genres =
  (json["genres"] as List).map((i) => new GenreTvShow.fromJson(i)).toList(),
        error = "";

  GenreTvshowResponse.withError(String errorValue)
      : genres = List(),
        error = errorValue;
}

