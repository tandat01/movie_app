class GenreTvShow {
  final int id;
  final String name;

  GenreTvShow(this.id,
      this.name);

  GenreTvShow.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"];
}
