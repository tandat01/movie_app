import 'package:movieapp/model/tvshow.dart';

class TvshowResponse {
  final List<TvShow> movies;
  final String error;

  TvshowResponse(this.movies, this.error);


  TvshowResponse.fromJson(Map<String, dynamic> json)
      : movies =
  (json["results"] as List).map((i) => new TvShow.fromJson(i)).toList(),
        error = "";

  TvshowResponse.withError(String errorValue)
      : movies = List(),
        error = errorValue;
}