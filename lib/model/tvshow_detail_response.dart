import 'package:movieapp/model/tvshow_detail.dart';

class TvshowDetailResponse {
  final TvshowDetail tvshowDetail;
  final String error;

  TvshowDetailResponse(this.tvshowDetail, this.error);

  TvshowDetailResponse.fromJson(Map<String, dynamic> json)
      : tvshowDetail = TvshowDetail.fromJson(json),
        error = "";

  TvshowDetailResponse.withError(String errorValue)
      : tvshowDetail = TvshowDetail(null, null, null, null),
        error = errorValue;
}