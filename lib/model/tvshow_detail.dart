import 'package:movieapp/model/tvshow/genre_tvshow.dart';
import 'package:movieapp/model/tvshow/network.dart';

class TvshowDetail{
  final int id ;
  final int gender;
  final List<GenreTvShow> gendertv;
  final List<NetworkTvshow> networktv;

  TvshowDetail(this.id, this.gender, this.gendertv, this.networktv);

  TvshowDetail.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        gender = json["gender"],
        gendertv = (json["genres"] as List).map((i) => new GenreTvShow.fromJson(i)).toList(),
        networktv = (json["networks"] as List).map((i) => new NetworkTvshow.fromJson(i)).toList();

}