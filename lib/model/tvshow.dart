class TvShow {
  final int id;
  final double popularity;
  final String name;
  final String backPoster;
  final String poster;
  final String overview;
  final double rating;

  TvShow(this.id, this.popularity, this.name,this.poster, this.backPoster, this.overview, this.rating);

  TvShow.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        popularity = json["popularity"],
        name = json["original_name"],
        backPoster = json["backdrop_path"],
        poster = json["poster_path"],
        overview = json["overview"],
        rating = json["vote_average"].toDouble();

}