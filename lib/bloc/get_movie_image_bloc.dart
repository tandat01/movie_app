import 'package:flutter/material.dart';
import 'package:movieapp/model/screen_shot.dart';
// import 'package:movieapp/model/movie_image.dart';
import 'package:movieapp/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieImageBloc{
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<Screenshot> _subject =
  BehaviorSubject<Screenshot>();


  getMovieImage(int id) async {

    Screenshot response = await _repository.getMovieImage(id);
    _subject.sink.add(response);

  }

  void drainStream(){ _subject.value = null; }
  @mustCallSuper
  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<Screenshot> get subject => _subject;
}
final nowMovieImage = MovieImageBloc();