import 'package:movieapp/model/movie_response.dart';
import 'package:movieapp/model/tvshow_response.dart';
import 'package:movieapp/repository/repository.dart';
import 'package:movieapp/repository/repositorytvshow.dart';
import 'package:rxdart/rxdart.dart';

class PopularListBloc {
  final TvShowRepository _repository = TvShowRepository();
  final BehaviorSubject<TvshowResponse> _subject =
  BehaviorSubject<TvshowResponse>();

  getMovies() async {
    // _repository.getLates();
    TvshowResponse response = await _repository.getLates();
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<TvshowResponse> get subject => _subject;

}
final latesTvshowBloc = PopularListBloc();