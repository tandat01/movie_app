

import 'package:flutter/material.dart';
import 'package:movieapp/model/tvshow_response.dart';
import 'package:movieapp/repository/repositorytvshow.dart';
import 'package:rxdart/rxdart.dart';

class SimilarMoviesBloc {
  final TvShowRepository _repository = TvShowRepository();
  final BehaviorSubject<TvshowResponse> _subject =
  BehaviorSubject<TvshowResponse>();

  getSimilarTvshow(int id) async {
    TvshowResponse response = await _repository.getSimilarTvshow(id);
    _subject.sink.add(response);
  }

  void drainStream(){ _subject.value = null; }
  @mustCallSuper
  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<TvshowResponse> get subject => _subject;

}
final similarTvshowBloc = SimilarMoviesBloc();