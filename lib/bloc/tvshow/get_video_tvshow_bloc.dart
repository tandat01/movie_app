
import 'package:flutter/material.dart';
import 'package:movieapp/model/video_response.dart';
import 'package:movieapp/repository/repositorytvshow.dart';
import 'package:rxdart/rxdart.dart';

class MovieVideosBloc {
  final TvShowRepository _repository = TvShowRepository();
  final BehaviorSubject<VideoResponse> _subject =
  BehaviorSubject<VideoResponse>();

  getMovieVideos(int id) async {
    VideoResponse response = await _repository.getTvshowVideos(id);
    _subject.sink.add(response);
  }

  void drainStream(){ _subject.value = null; }
  @mustCallSuper
  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<VideoResponse> get subject => _subject;

}
final tvshowVideosBloc = MovieVideosBloc();