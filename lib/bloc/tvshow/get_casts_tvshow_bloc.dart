
import 'package:flutter/material.dart';
import 'package:movieapp/model/cast_response.dart';
import 'package:movieapp/repository/repositorytvshow.dart';
import 'package:rxdart/rxdart.dart';

class CastsBloc {
  final TvShowRepository _repository = TvShowRepository();
  final BehaviorSubject<CastResponse> _subject =
  BehaviorSubject<CastResponse>();

  getCasts(int id) async {
    CastResponse response = await _repository.getCastsTvshow(id);
    _subject.sink.add(response);
  }

  void drainStream(){ _subject.value = null; }
  @mustCallSuper
  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<CastResponse> get subject => _subject;

}
final castsTvshowBloc = CastsBloc();