import 'package:dio/dio.dart';
import 'package:movieapp/model/cast_response.dart';
import 'package:movieapp/model/tvshow_response.dart';
import 'package:movieapp/model/video_response.dart';

class TvShowRepository{
  final String apiKey = "4f3840f44ce67ac2971a66799b5f5046";
  static String mainUrl = "https://api.themoviedb.org/3";
  final Dio _dio = Dio();
  var getPopularUrl = '$mainUrl/tv/popular';
  var getOnTheAirUrl = '$mainUrl/tv/on_the_air';
  var getTodayUrl = '$mainUrl/tv/airing_today';
  var getLatestUrl = '$mainUrl/tv/latest';
  var getTopRatedUrl = '$mainUrl/tv/top_rated';
  var TvshowUrl = "$mainUrl/tv";

  Future<TvshowResponse> getPopular() async {
    var params = {
      "api_key": apiKey,
      "language": "en-US",
      "page": 1
    };
    try {
      Response response = await _dio.get(getPopularUrl, queryParameters: params);
      return TvshowResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return TvshowResponse.withError("$error");
    }
  }

  Future<TvshowResponse> getTheAir() async {
    var params = {
      "api_key": apiKey,
      "language": "en-US",
    };
    try {
      Response response = await _dio.get(getOnTheAirUrl, queryParameters: params);
      // print(response.data);
      return TvshowResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return TvshowResponse.withError("$error");
    }
  }

  Future<TvshowResponse> getToday() async {
    var params = {
      "api_key": apiKey,
      "language": "en-US",
      "page": 1
    };
    try {
      Response response = await _dio.get(getTodayUrl, queryParameters: params);
      return TvshowResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return TvshowResponse.withError("$error");
    }
  }

  Future<TvshowResponse> getLates() async {
    var params = {
      "api_key": apiKey,
      "language": "en-US"
    };
    try {
      Response response = await _dio.get(getLatestUrl, queryParameters: params);
      // print(response.data);
      return TvshowResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return TvshowResponse.withError("$error");
    }
  }

  Future<TvshowResponse> getTopRated() async {
    var params = {
      "api_key": apiKey,
      "language": "en-US",
      "page": 1
    };
    try {
      Response response = await _dio.get(getTopRatedUrl, queryParameters: params);
      return TvshowResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return TvshowResponse.withError("$error");
    }
  }



  Future<TvshowResponse> getSimilarTvshow(int id) async {
    var params = {
      "api_key": apiKey,
      "language": "en-US"
    };
    try {
      Response response = await _dio.get(TvshowUrl + "/$id" + "/similar", queryParameters: params);
      return TvshowResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return TvshowResponse.withError("$error");
    }
  }

  Future<CastResponse> getCastsTvshow(int id) async {
    var params = {
      "api_key": apiKey,
      "language": "en-US"
    };
    try {
      Response response = await _dio.get(TvshowUrl + "/$id" + "/credits", queryParameters: params);
      return CastResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return CastResponse.withError("$error");
    }
  }

  Future<VideoResponse>   getTvshowVideos(int id) async {
    var params = {
      "api_key": apiKey,
      "language": "en-US"
    };
    try {
      Response response = await _dio.get(TvshowUrl + "/$id" + "/videos", queryParameters: params);
      return VideoResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return VideoResponse.withError("$error");
    }
  }
}