import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movieapp/bloc/tvshow/get_popular.dart';
import 'package:movieapp/bloc/tvshow/get_video_tvshow_bloc.dart';
import 'package:movieapp/model/tvshow.dart';
import 'package:movieapp/model/tvshow_response.dart';
import 'package:movieapp/model/video.dart';
import 'package:movieapp/model/video_response.dart';
import 'package:movieapp/ui/tab1/video_player.dart';
import 'package:movieapp/ui/tab1/widgets/casts.dart';
import 'package:movieapp/ui/tab1/widgets/similar_movies.dart';
import 'package:movieapp/ui/tab3/widgets/casts_tvshow.dart';
import 'package:movieapp/ui/tab3/widgets/similar_tvshow.dart';
import 'package:sliver_fab/sliver_fab.dart';
import 'package:movieapp/style/theme.dart' as Style;
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class DetailtvShow extends StatefulWidget {

  final TvShow tvShow;

  const DetailtvShow({Key key, this.tvShow}) : super(key: key);


  @override
  _DetailtvShowState createState() => _DetailtvShowState(tvShow);
}
class _DetailtvShowState extends State<DetailtvShow> {

  final TvShow tvShow;

  _DetailtvShowState(this.tvShow);

  @override
  void initState() {
    super.initState();
    tvshowVideosBloc..getMovieVideos(tvShow.id);
  }

  @override
  void dispose() {
    super.dispose();
    tvshowVideosBloc..drainStream();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      body: new Builder(
        builder: (context) {
          return new SliverFab(
            floatingPosition: FloatingPosition(right: 20,top: -160),
            floatingWidget: StreamBuilder<VideoResponse>(
              stream: tvshowVideosBloc.subject.stream,
              builder: (context, AsyncSnapshot<VideoResponse> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.error != null &&
                      snapshot.data.error.length > 0) {
                    return _buildErrorWidget(snapshot.data.error);
                  }
                   // return Container();
                  return _buildVideoWidget(snapshot.data);
                } else if (snapshot.hasError) {
                  return _buildErrorWidget(snapshot.error);
                } else {
                  return _buildLoadingWidget();
                }
              },
            ),
            expandedHeight: 200.0,
            slivers: <Widget>[
              new SliverAppBar(
                  backgroundColor: Style.Colors.mainColor,
                  expandedHeight: 300.0,
                  pinned: true,
                  flexibleSpace: new FlexibleSpaceBar(
                      background: Stack(
                        children: <Widget>[
                          Container(
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      "https://image.tmdb.org/t/p/original/" +
                                          tvShow.backPoster)),
                            ),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Colors.black.withOpacity(0.6)),
                            ),
                          ),
                          Positioned(
                            bottom: 0.0,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10,bottom: 3),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      decoration: new BoxDecoration(
                                        color: const Color(0xff7c94b6),
                                        shape: BoxShape.rectangle,
                                        image: new DecorationImage(
                                            fit: BoxFit.cover,
                                            // colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                                            image: NetworkImage(
                                                "https://image.tmdb.org/t/p/original/" +
                                                    tvShow.poster)),
                                      ),
                                      height: 150,
                                      width: 100,
                                      child: new Container(
                                        decoration: new BoxDecoration(
                                            color: Colors.black.withOpacity(0.2)),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 15),
                                        child: Text(
                                          tvShow.name.length > 40
                                              ? tvShow.name.substring(0, 37) + "..."
                                              : tvShow.name,
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 15 ),
                                        child: Row(
                                          children: [
                                            Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              children: <Widget>[
                                                RatingBar(
                                                  itemSize: 10.0,
                                                  initialRating: tvShow.rating / 2,
                                                  minRating: 1,
                                                  direction: Axis.horizontal,
                                                  allowHalfRating: true,
                                                  itemCount: 1,
                                                  itemPadding: EdgeInsets.symmetric(
                                                      horizontal: 2.0),
                                                  itemBuilder: (context, _) => Icon(
                                                    EvaIcons.star,
                                                    color: Style.Colors.secondColor,
                                                  ),
                                                  onRatingUpdate: (rating) {
                                                    print(rating);
                                                  },
                                                ),
                                                SizedBox(
                                                  width: 5.0,
                                                ),
                                                Text(
                                                  tvShow.rating.toString(),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14.0,
                                                      fontWeight: FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Text(
                                              "March 24,2021",
                                              style: TextStyle(
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 5),
                                        child: Row(
                                          children: [
                                            GestureDetector(
                                              child: Container(
                                                margin: EdgeInsets.only(right: 10),
                                                height: 40,
                                                width: 110,
                                                // color: const Color(0xff7c94b6),
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(10),
                                                  color: Style.Colors.mainColor.withOpacity(0.7),
                                                ),
                                                child: Row(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Icon(Icons.add,color: Colors.white,),
                                                    SizedBox(width: 10),
                                                    Text("My List",style: TextStyle(
                                                      fontSize: 12.0,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.white,
                                                    ),),
                                                  ],
                                                ),
                                              ),
                                              onTap: (){},
                                            ),
                                            GestureDetector(
                                              child: Container(
                                                height: 40,
                                                width: 110,
                                                // color: const Color(0xff7c94b6),
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(10),
                                                  color: Style.Colors.mainColor.withOpacity(0.7),
                                                ),
                                                child: Row(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Icon(Icons.panorama,color: Colors.redAccent,size: 20,),
                                                    SizedBox(width: 10,),
                                                    Text("Gallery",style: TextStyle(
                                                      fontSize: 12.0,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.white,
                                                    ),),
                                                  ],
                                                ),
                                              ),
                                              onTap: (){
                                                // Navigator.push(
                                                //   context,
                                                //   MaterialPageRoute(
                                                //     builder: (context) => MovieGalleryScreen(screenshotId: movie.id, movie: movie,
                                                //     ),
                                                //   ),
                                                // );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ))),
              SliverPadding(
                  padding: EdgeInsets.all(0.0),
                  sliver: SliverList(
                      delegate: SliverChildListDelegate([

                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, top: 20.0),
                          child: Text(
                            "Description",
                            style: TextStyle(
                                color: Style.Colors.titleColor,
                                fontWeight: FontWeight.w500,
                                fontSize: 12.0),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            tvShow.overview,
                            style: TextStyle(
                                color: Colors.white, fontSize: 12.0, height: 1.5),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        // MovieInfo(
                        //   id: movie.id,
                        // ),
                        CastsTvshow(id: tvShow.id),
                        SimilarTvshow(id: tvShow.id),
                      ])))
            ],
          );
        },
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [],
        ));
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Error occured: $error"),
          ],
        ));
  }

  Widget _buildVideoWidget(VideoResponse data) {
    List<Video> videos = data.videos;
    return FloatingActionButton(
      backgroundColor: Style.Colors.secondColor,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => VideoPlayerScreen(
              controller: YoutubePlayerController(
                initialVideoId: videos[0].key,
                flags: YoutubePlayerFlags(
                  autoPlay: true,
                  mute: true,
                ),
              ),
            ),
          ),
        );
      },
      child: Icon(Icons.play_arrow),
    );
  }
}
