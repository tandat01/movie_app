import 'package:flutter/material.dart';
import 'package:movieapp/style/theme.dart' as Style;
import 'package:movieapp/ui/tab3/widgets/lates.dart';
import 'package:movieapp/ui/tab3/widgets/on_the_air.dart';
import 'package:movieapp/ui/tab3/widgets/popular_tvshow.dart';
import 'package:movieapp/ui/tab3/widgets/today.dart';
import 'package:movieapp/ui/tab3/widgets/top_rated_tvshow.dart';
class Tab3Page extends StatefulWidget {
  @override
  _Tab3PageState createState() => _Tab3PageState();
}

class _Tab3PageState extends State<Tab3Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      appBar: AppBar(
        backgroundColor: Style.Colors.mainColor,
        title: Text("TV Show"),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          OnTheAirWidget(),
          PopularTvShowWidget(),
          TodayWidget(),
          TopratedTVShowWidget(),
        ],
      ),
    );
  }
}
