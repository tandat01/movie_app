import 'dart:convert';

import 'package:http/http.dart' as http ;
import 'package:flutter/material.dart';
import 'package:movieapp/model/movie.dart';
import 'package:movieapp/repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';



class Tab5Page extends StatefulWidget {
  @override
  _Tab5PageState createState() => _Tab5PageState();
}

class _Tab5PageState extends State<Tab5Page> {
  List<Movie> movies = List();
  bool hasLoaded = true;
  var key = ' 4f3840f44ce67ac2971a66799b5f5046';

  final PublishSubject subject = PublishSubject<String>();

  @override
  void dispose() {
    subject.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    subject.stream..listen(searchMovies);
  }

  void searchMovies(query) {
    resetMovies();
    if (query.isEmpty) {
      setState(() {
        hasLoaded = true;
      });
      return; //Forgot to add in the tutorial <- leaves function if there is no query in the box.
    }
    setState(() => hasLoaded = false);
    MovieRepository().getSearchMovie()
         .then((res) => (res.body))
        .then((map) => map["results"])
        .then((movies) => movies.forEach(addMovie))
        .catchError(onError)
        .then((e) {
      setState(() {
        hasLoaded = true;
      });
    });
  }

  void onError(dynamic d) {
    setState(() {
      hasLoaded = true;
    });
  }

  void addMovie(item) {
    setState(() {
      movies.add(Movie.fromJson(item));
    });
    print('${movies.map((m) => m.title)}');
  }

  void resetMovies() {
    setState(() => movies.clear());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie Searcher'),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            TextField(
              onChanged: (String string) => (subject.add(string)),
            ),
            hasLoaded ? Container() : CircularProgressIndicator(),
            Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.all(10.0),
                  itemCount: movies.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new MovieView(movies[index]);
                  },
                ))
          ],
        ),
      ),
    );
  }
}

class MovieView extends StatefulWidget {
  MovieView(this.movie);
  final Movie movie;

  @override
  MovieViewState createState() => MovieViewState();
}

class MovieViewState extends State<MovieView> {
  Movie movieState;

  @override
  void initState() {
    super.initState();
    movieState = widget.movie;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Container(
            height: 200.0,
            padding: EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                movieState.poster != null
                    ? Hero(
                  child: Image.network(
                      "https://image.tmdb.org/t/p/w92${movieState.poster}"),
                  tag: movieState.id,
                )
                    : Container(),
                Expanded(
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              movieState.title,
                              maxLines: 10,
                            ),
                          ),
                        ),
                        // Align(
                        //   alignment: Alignment.topRight,
                        //   child: IconButton(
                        //     icon: movieState.favored
                        //         ? Icon(Icons.star)
                        //         : Icon(Icons.star_border),
                        //     color: Colors.white,
                        //     onPressed: () {},
                        //   ),
                        // ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: IconButton(
                            icon: Icon(Icons.arrow_downward),
                            color: Colors.white,
                            onPressed: () {},
                          ),
                        )
                      ],
                    ))
              ],
            )));
  }
}