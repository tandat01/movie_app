import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:movieapp/style/theme.dart' as Style;
import 'package:movieapp/ui/tab1/widgets/best_movies.dart';
import 'package:movieapp/ui/tab1/widgets/genres.dart';
import 'package:movieapp/ui/tab1/widgets/hot.dart';
import 'package:movieapp/ui/tab1/widgets/hot_movie.dart';
import 'package:movieapp/ui/tab1/widgets/now_playing.dart';
import 'package:movieapp/ui/tab1/widgets/persons.dart';
import 'package:movieapp/ui/tab2/widgets/now_playing.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with  AutomaticKeepAliveClientMixin{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      // appBar: AppBar(
      //   backgroundColor: Style.Colors.mainColor,
      //   centerTitle: true,
      //   leading: Icon(EvaIcons.menu2Outline, color: Colors.white,),
      //   title: Text("Movie App"),
      //   actions: <Widget>[
      //     IconButton(
      //       onPressed: () {},
      //       icon: Icon(EvaIcons.searchOutline, color: Colors.white,)
      //     )
      //   ],
      // ),
      body: ListView(
        children: <Widget>[
          NowPlaying(),
          NowPlayingWidget(),
          // PersonsList(),
          BestMovies(),
          GenresScreen(),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}