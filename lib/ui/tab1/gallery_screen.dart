import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movieapp/bloc/get_movie_image_bloc.dart';
import 'package:movieapp/model/movie.dart';

import 'package:movieapp/style/theme.dart' as Style;
import 'package:movieapp/model/screen_shot.dart';
import 'package:movieapp/repository/repository.dart';
import 'package:movieapp/style/theme.dart';



class MovieGalleryScreen extends StatefulWidget {
  final screenshotId;
  final Movie movie;

  MovieGalleryScreen(
      {Key key, @required this.screenshotId, @required this.movie})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _MovieGalleryScreenState(movie);
}

class _MovieGalleryScreenState extends State<MovieGalleryScreen> {
  final Movie _movie;

  List data = List();

  _MovieGalleryScreenState(this._movie);

  @override
  void initState() {
    // ignore: unnecessary_statements
    // widget.screenshotId;
    // print("oke ${widget.screenshotId}");
    MovieRepository().getMovieImage(widget.screenshotId).then((value) {
      // print("aaaa${value}");
      //add vao list thi nhu nay
      data.addAll(value['posters']);
      setState(() {});
    }).catchError((onError) {
      print(onError);
    });
    // print(nowMovieImage..getMovieImage(widget.screenshotId));
    super.initState();
    // nowMovieImage..getMovieImage(widget.screenshotId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      appBar: AppBar(
        backgroundColor: Style.Colors.mainColor,
        title: Text(
          _movie.title.length > 40
              ? _movie.title.substring(0, 37) + "..."
              : _movie.title,
          style: TextStyle(
            fontSize: 17.0,
            fontWeight: FontWeight.bold,

          ),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(left: 5,right: 5),
        // width: 50,
        child: GridView.builder(
          itemCount: data.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 0.1,
              mainAxisSpacing: 0.1,
            childAspectRatio: 0.6

          ),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            var items = data[index];
            return Container(
              child: Card(
                borderOnForeground: true,
                shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image(
                    image: NetworkImage(
                        "https://image.tmdb.org/t/p/w200/" + items['file_path']),
                    fit: BoxFit.cover,
                    width: 120,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
