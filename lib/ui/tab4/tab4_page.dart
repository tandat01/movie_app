import 'package:flutter/material.dart';
import 'package:movieapp/style/theme.dart' as Style;

class Tab4Page extends StatefulWidget {
  @override
  _Tab4PageState createState() => _Tab4PageState();
}

class _Tab4PageState extends State<Tab4Page> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 2,
      child: Scaffold(
        backgroundColor: Style.Colors.mainColor,
        appBar: AppBar(
          backgroundColor: Style.Colors.mainColor,
          bottom: const TabBar(
            tabs: <Widget>[
              Tab(
                text: "Movie",
              ),
              Tab(
               text: "TV Show",
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: <Widget>[
            Center(
              child: Text('It\'s cloudy here'),
            ),
            Center(
              child: Text('It\'s rainy here'),
            ),
          ],
        ),
      ),
    );
  }
}
