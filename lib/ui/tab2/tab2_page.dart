import 'package:flutter/material.dart';
import 'package:movieapp/style/theme.dart' as Style;
import 'package:movieapp/ui/tab1/widgets/now_playing.dart';
import 'package:movieapp/ui/tab2/widgets/now_playing.dart';
import 'package:movieapp/ui/tab2/widgets/popular.dart';
import 'package:movieapp/ui/tab2/widgets/top_rated.dart';
import 'package:movieapp/ui/tab2/widgets/upcoming.dart';

class Tab2Page extends StatefulWidget {
  @override
  _Tab2PageState createState() => _Tab2PageState();
}

class _Tab2PageState extends State<Tab2Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      appBar: AppBar(
        backgroundColor: Style.Colors.mainColor,
        title: Text("Movie"),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          PopularPage(),
          Upcoming(),
          TopRated(),
          NowPlayingWidget(),
        ],
      ),
    );
  }
}
