import 'package:flutter/material.dart';
import 'package:movieapp/ui/tab1/home_screen.dart';
import 'package:movieapp/ui/tab2/tab2_page.dart';
import 'package:movieapp/ui/tab3/tab3_page.dart';
import 'package:movieapp/ui/tab4/tab4_page.dart';
import 'package:movieapp/ui/tab5/tab5_page.dart';

class MyScreen extends StatefulWidget {
  @override
  _MyScreenState createState() => _MyScreenState();
}

class _MyScreenState extends State<MyScreen> {
  int currentTabIndex = 0 ;

  List<Widget> pages;
  Widget currentPage;

  HomeScreen homeScreen;
  Tab2Page tab2page;
  Tab3Page tab3page;
  Tab4Page tab4page;
  Tab5Page tab5page;
  @override
  void initState() {
    // TODO: implement initState
    homeScreen = HomeScreen();
    tab2page = Tab2Page();
    tab3page = Tab3Page();
    tab4page =Tab4Page();
    tab5page = Tab5Page();
    pages=[homeScreen,tab2page,tab3page,tab4page,tab5page];

    currentPage=homeScreen;


    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: (int index){
          setState(() {
            currentTabIndex = index;
            currentPage=pages[index];
          });
        },
        currentIndex: currentTabIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Color(0xfff191919),
        selectedItemColor: Colors.red,
        unselectedItemColor: Colors.grey,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Text("Home"),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.motion_photos_on),
            title: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Text("Movie"),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.computer_sharp),
            title: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Text("TV Shows"),
            ),
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.list_outlined),
          //   title: Padding(
          //     padding: const EdgeInsets.only(top: 5),
          //     child: Text("Watchlist"),
          //   ),
          // ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.settings),
          //   title: Padding(
          //     padding: const EdgeInsets.only(top: 5),
          //     child: Text("Settings"),
          //   ),
          // ),
        ],
      ),
      body: currentPage,
    );
  }
}